module gitlab.com/aleph-project/language

require (
	github.com/alecthomas/assert v0.0.0-20170929043011-405dbfeb8e38
	github.com/alecthomas/repr v0.0.0-20181024024818-d37bc2a10ba1 // indirect
	gitlab.com/aleph-project/form v0.2.3
	golang.org/x/sys v0.0.0-20181121002834-0cf1ed9e522b // indirect
)
