package parser

import "gitlab.com/aleph-project/form"

func (p *Parser) newHead(head string) form.Head {
	return form.NewSymbolicHead(head, p.context)
}

func (p *Parser) newForm(head form.Head, els ...form.Form) form.Form {
	return form.Must(form.New(head, els...))
}

func (p *Parser) newNamedForm(head string, els ...form.Form) form.Form {
	return p.newForm(p.newHead(head), els...)
}
