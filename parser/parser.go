package parser

import (
	"fmt"
	"math"
	"strconv"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/language/scanner"
	"gitlab.com/aleph-project/language/token"
)

type Mode uint

const (
	// ParseDefault ignores comments and terminates after 10 errors
	ParseDefault Mode = 0
	// ParseComments includes comments
	ParseComments Mode = 1 << iota
	// AllErrors includes all errors
	AllErrors
	// noBailOut disables bailout catching ** FOR TESTING PURPOSES ONLY
	noBailOut
)

// IsSet returns whether mode N is set on mode M
func (m Mode) IsSet(n Mode) bool { return m&n != 0 }

// IsUnset returns whether mode N is not set on mode M
func (m Mode) IsUnset(n Mode) bool { return m&n == 0 }

// A bailout panic is raised to indicate early termination.
type bailout struct{}

// a close function is called to set the end marker of a node
type close func(pos int)

type Parser struct {
	scanner scanner.Scanner
	errors  scanner.ErrorList

	mode Mode
	// comments []*Value
	close   close
	context form.Context

	pos int
	tok token.Token
	lit string
}

func (p *Parser) Init(src []byte, mode Mode, context form.Context) {
	var m = scanner.ScanDefault
	if mode.IsSet(ParseComments) {
		m |= scanner.ScanComments
	}

	p.scanner.Init(src, m, p.error)

	p.mode = mode
	p.close = nil
	p.context = context

	p.errors.Clear()
	p.next()
}

func (p *Parser) next() {
	for {
		p.pos, p.tok, p.lit = p.scanner.Scan()
		if p.close != nil {
			p.close(p.pos)
			p.close = nil
		}

		if p.tok == token.WHITESPACE {
			continue
		}

		if p.tok != token.COMMENT && ParseComments.IsUnset(p.mode) {
			return
		}

		// c := &Value{
		// 	Node: Node{
		// 		Pos:  p.pos,
		// 		Kind: COMMENT,
		// 	},
		// 	Str:   p.lit,
		// 	Value: nil,
		// }
		// p.comments = append(p.comments, c)
		// p.close = c.Close
	}
}

func (p *Parser) error(pos int, msg string) {
	if AllErrors.IsUnset(p.mode) && len(p.errors) >= 10 {
		panic(bailout{})
	}

	p.errors.Add(pos, msg)
}

func (p *Parser) final(e interface{}) scanner.ErrorList {
	if e == nil {
		return p.errors
	}

	// resume same panic if it's not a bailout
	if _, ok := e.(bailout); !ok {
		panic(e)
	}

	p.errors.Add(p.pos, "Too many errors... (more than 10)")
	return p.errors
}

func (p *Parser) Parse() (expr form.Form, errs scanner.ErrorList) {
	if p.mode.IsUnset(noBailOut) {
		defer func() {
			errs = p.final(recover())
		}()
	}

	return p.parseExpression(false), p.errors
}

func (p *Parser) ParseAll() (expr form.Form, errs scanner.ErrorList) {
	if p.mode.IsUnset(noBailOut) {
		defer func() {
			errs = p.final(recover())
		}()
	}

	var exprs = []form.Form{}
	for p.tok != token.EOF && p.tok != token.ILLEGAL {
		if p.tok == token.NEWLINE {
			p.next()
			continue
		}

		exprs = append(exprs, p.parseExpression(false))
	}

	return p.newNamedForm("CompoundExpression", exprs...), p.errors
}

func (p *Parser) parseDelimitedExpressions(delimiter, end token.Token) []form.Form {
	var args = []form.Form{}

	if p.tok == end {
		p.next()
		return args
	}

	if p.tok == token.EOF || p.tok == token.ILLEGAL {
		p.error(p.pos, fmt.Sprintf("unexpected %v", p.tok))
		return args
	}

	for {
		arg := p.parseExpression(true)
		args = append(args, arg)

		if p.tok == end {
			p.next()
			break
		}

		if p.tok != delimiter {
			p.error(p.pos, fmt.Sprintf("looking for token %v, found %v", end, p.tok))
			break
		}

		p.next()
	}

	return args
}

func (p *Parser) parseExpression(ignoreNewLines bool) form.Form {
	var pos = p.pos
	var ops = []*operation{}
	unshift := func(op *Operator, values ...form.Form) {
		ops = append([]*operation{&operation{op, values}}, ops...)
	}

	var value form.Form
	var prefixes, postfixes []*Operator
	for {
		prefixes, value, postfixes = p.parseExpressionFragment()
		// result: either prefixes or postfixes
		for len(ops) > 0 && len(postfixes) > 0 {
			if ops[0].op.Precedence > postfixes[0].Precedence {
				value, ops = form.Must(ops[0].op.Apply(append(ops[0].values, value)...)), ops[1:]
				continue
			}

			if ops[0].op.Precedence < postfixes[0].Precedence {
				value, postfixes = form.Must(postfixes[0].Apply(value)), postfixes[1:]
				continue
			}

			value, ops = form.Must(ops[0].op.Apply(append(ops[0].values, value)...)), ops[1:]
		}
		// result: either (ops and/or prefixes) or postfixes

		n := len(prefixes)
		if n > 0 {
			ops = append(make([]*operation, n, n+len(ops)), ops...)
			for i, p := range prefixes {
				ops[n-i-1] = &operation{p, []form.Form{}}
			}
		}
		// result: either ops or postfixes

		if p.tok == token.NEWLINE {
			p.next()
			if !ignoreNewLines {
				// TODO this probably isn't the best way of handling list
				for len(postfixes) > 0 {
					value, postfixes = form.Must(postfixes[0].Apply(value)), postfixes[1:]
				}
				break
			}
		}

		op, ok := p.parseOperator(InfixOperator)
		for len(postfixes) > 0 {
			value, postfixes = form.Must(postfixes[0].Apply(value)), postfixes[1:]
		}
		if !ok {
			if p.tok != token.LPAREN && !p.nextIsValue() {
				break
			}
			op = Multiply
		}
		// result: only ops and new op

		for len(ops) > 0 && op.Precedence < ops[0].op.Precedence {
			value, ops = form.Must(ops[0].op.Apply(append(ops[0].values, value)...)), ops[1:]
		}
		if len(ops) > 0 && op == ops[0].op {
			ops[0].values = append(ops[0].values, value)
		} else if len(ops) > 0 && op.Precedence == ops[0].op.Precedence {
			value, ops = form.Must(ops[0].op.Apply(append(ops[0].values, value)...)), ops[1:]
			unshift(op, value)
		} else { // len(ops) == 0 || op.Precedence > ops[0].op.Precedence
			unshift(op, value)
		}
	}
	// result: only ops

	for len(ops) > 0 {
		value, ops = form.Must(ops[0].op.Apply(append(ops[0].values, value)...)), ops[1:]
	}
	if p.pos == pos {
		p.error(pos, fmt.Sprintf("unexpected token at char %d, token %v, literal %s", p.pos, p.tok, strconv.Quote(p.lit)))
		p.next()
	}
	return value
}

func (p *Parser) parseExpressionFragment() ([]*Operator, form.Form, []*Operator) {
	var prefixes = p.parseOperators(PrefixOperator)

	// reverse prefixes
	n := len(prefixes)
	for i := 0; i < n/2; i++ {
		j := n - i - 1
		prefixes[j], prefixes[i] = prefixes[i], prefixes[j]
	}

	value := p.parseOperand()

	var postfixes = p.parseOperators(PostfixOperator)

	for len(prefixes) > 0 && len(postfixes) > 0 {
		if prefixes[0].Precedence > postfixes[0].Precedence {
			value, prefixes = form.Must(prefixes[0].Apply(value)), prefixes[1:]
			continue
		}

		if prefixes[0].Precedence < postfixes[0].Precedence {
			value, postfixes = form.Must(postfixes[0].Apply(value)), postfixes[1:]
			continue
		}

		value, prefixes = form.Must(prefixes[0].Apply(value)), prefixes[1:]
	}

	return prefixes, value, postfixes
}

func (p *Parser) parseOperator(kind OperatorKind) (*Operator, bool) {
	op, ok := Operators[kind][p.tok]
	if !ok {
		return nil, false
	}

	p.next()
	return op.Complete(p), true
}

func (p *Parser) parseOperators(kind OperatorKind) []*Operator {
	var ops = []*Operator{}

	for {
		op, ok := p.parseOperator(kind)
		if !ok {
			return ops
		}

		ops = append(ops, op)
	}
}

func (p *Parser) parseOperand() form.Form {
	v, ok := p.parseValue()
	if ok {
		return v
	}

	if p.tok == token.NEWLINE {
		return form.Null
	}

	if p.tok == token.LPAREN {
		p.next()
		exprs := p.parseDelimitedExpressions(token.ILLEGAL, token.RPAREN)
		if len(exprs) == 0 {
			return form.Null
		}
		if len(exprs) == 1 {
			return exprs[0]
		}
		panic("this should never happen")
	}

	if p.tok == token.LBRACE {
		p.next()
		exprs := p.parseDelimitedExpressions(token.COMMA, token.RBRACE)
		return form.ListForm(exprs)
	}

	p.error(p.pos, fmt.Sprintf("expected operand, got %v", p.tok))
	return form.Null
}

func (p *Parser) nextIsValue() bool {
	switch p.tok {
	case token.SYMBOL, token.NULL, token.INF, token.INTEGER, token.REAL, token.STRING, token.RAW_STRING:
		return true

	default:
		return false
	}
}

func (p *Parser) parseValue() (f form.Form, ok bool) {
	var b form.Head

	switch p.tok {
	case token.SYMBOL:
		f = p.processSymbol(p.lit)
		p.next()

		switch p.tok {
		case token.BLANK:
			b = p.newHead("Blank")
		case token.BLANK_SEQ:
			b = p.newHead("BlankSequence")
		case token.BLANK_NULL_SEQ:
			b = p.newHead("BlankNullSequence")

		default:
			return f, true
		}
		p.next()

		switch p.tok {
		case token.SYMBOL:
			h := p.processSymbol(p.lit)
			p.next()
			return p.newNamedForm("Pattern", f, p.newForm(b, h)), true

		case token.DOT:
			p.next()
			return p.newNamedForm("Optional", p.newNamedForm("Pattern", f, p.newForm(b))), true

		default:
			return p.newNamedForm("Pattern", f, p.newForm(b)), true
		}

	case token.BLANK:
		b = p.newHead("Blank")
		goto blank
	case token.BLANK_SEQ:
		b = p.newHead("BlankSequence")
		goto blank
	case token.BLANK_NULL_SEQ:
		b = p.newHead("BlankNullSequence")
		goto blank

	case token.NULL:
		f = form.Null
	case token.INF:
		f = form.SmallRealForm(math.Inf(+1))

	case token.INTEGER:
		f = p.processInteger(p.lit)
	case token.REAL:
		f = p.processReal(p.lit)
	case token.STRING:
		f = p.processString(p.lit)
	case token.RAW_STRING:
		f = p.processRawString(p.lit)

	default:
		return nil, false
	}

	p.next()
	return f, true

blank:
	p.next()
	switch p.tok {
	case token.SYMBOL:
		h := p.processSymbol(p.lit)
		p.next()
		return p.newForm(b, h), true

	case token.DOT:
		p.next()
		return p.newNamedForm("Optional", p.newForm(b)), true

	default:
		return p.newForm(b), true
	}
}

func (p *Parser) processSymbol(lit string) form.PrimitiveForm {
	switch lit {
	case "True":
		return form.BooleanForm(true)

	case "False":
		return form.BooleanForm(false)

	case "Null":
		return form.Null

	default:
		return form.SymbolForm(lit)
	}
}

func (p *Parser) processString(lit string) form.StringForm {
	v, err := strconv.Unquote(lit)
	if err != nil {
		p.error(p.pos, err.Error())
	}
	return form.StringForm(v)
}

func (p *Parser) processRawString(lit string) form.StringForm {
	n := len(lit)
	return form.StringForm(lit[1 : n-1])
}

func (p *Parser) processInteger(lit string) form.NumericForm {
	v, err := strconv.ParseInt(lit, 0, 64)
	if err != nil {
		p.error(p.pos, err.Error())
	}
	return form.SmallIntegerForm(v)
}

func (p *Parser) processReal(lit string) form.NumericForm {
	v, err := strconv.ParseFloat(lit, 64)
	if err != nil {
		p.error(p.pos, err.Error())
	}
	return form.SmallRealForm(v)
}
