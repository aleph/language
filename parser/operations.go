package parser

import (
	"errors"
	"fmt"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/language/token"
)

type operation struct {
	op     *Operator
	values []form.Form
}

type OperatorKind int

const (
	NotAnOperator OperatorKind = iota
	PrefixOperator
	InfixOperator
	PostfixOperator
)

type Associativity int

const (
	Unassociative Associativity = iota
	PolyAssociative
	LeftAssociative
	RightAssociative
)

type Applicator func(...form.Form) (form.Form, error)
type Completer func(*Operator, *Parser) *Operator

type Operator struct {
	Kind          OperatorKind
	Head          form.Head
	Precedence    int
	Associativity Associativity
	Applicator    Applicator
	Completer     Completer
	Token         token.Token
}

func (op *Operator) Complete(p *Parser) *Operator {
	if op.Completer == nil {
		return op
	}
	return op.Completer(op, p)
}

func newOp() *Operator {
	return new(Operator)
}

func (op *Operator) Apply(values ...form.Form) (form.Form, error) {
	if op.Associativity == PolyAssociative || len(values) < 3 {
		return op.Applicator(values...)
	}

	if op.Associativity == LeftAssociative {
		return ApplyLeft(op.Applicator, values...)
	}

	if op.Associativity == RightAssociative {
		return ApplyRight(op.Applicator, values...)
	}

	panic("invalid associativity for more than 2 arguments")
}

func ApplyLeft(fn Applicator, values ...form.Form) (form.Form, error) {
	if len(values) == 0 {
		return fn()
	}

	var err error
	var v = values[0]
	for _, u := range values[1:] {
		v, err = fn(v, u)
		if err != nil {
			return nil, err
		}
	}
	return v, nil
}

func ApplyRight(fn Applicator, values ...form.Form) (form.Form, error) {
	if len(values) == 0 {
		return fn()
	}

	var err error
	var v = values[len(values)-1]
	for i := len(values) - 2; i >= 0; i-- {
		v, err = fn(values[i], v)
		if err != nil {
			return nil, err
		}
	}
	return v, nil
}

func (op *Operator) prefix() *Operator  { op.Kind = PrefixOperator; return op }
func (op *Operator) infix() *Operator   { op.Kind = InfixOperator; return op }
func (op *Operator) postfix() *Operator { op.Kind = PostfixOperator; return op }

func (op *Operator) name(head string) *Operator { return op.head(form.NewUnknownSymbolicHead(head)) }
func (op *Operator) apply(apply Applicator) *Operator {
	op.Applicator = apply
	return op
}
func (op *Operator) head(head form.Head) *Operator {
	return op.apply(func(els ...form.Form) (form.Form, error) {
		return form.New(head, els...)
	})
}

func (op *Operator) complete(c Completer) *Operator { op.Completer = c; return op }
func (op *Operator) completeDelimited(delimiter, end token.Token) *Operator {
	return op.complete(func(op *Operator, p *Parser) *Operator {
		var next = *op // copy
		var args = p.parseDelimitedExpressions(delimiter, end)
		return (&next).apply(func(els ...form.Form) (form.Form, error) {
			els = append(els, args...)
			return op.Apply(els...)
		})
	})
}

func (op *Operator) poly() *Operator  { op.Associativity = PolyAssociative; return op }
func (op *Operator) left() *Operator  { op.Associativity = LeftAssociative; return op }
func (op *Operator) right() *Operator { op.Associativity = RightAssociative; return op }

func (op *Operator) token(tok token.Token) *Operator {
	op.Token = tok
	return op
}

func completePart(op *Operator, p *Parser) *Operator {
	var next = *op // copy
	var args = p.parseDelimitedExpressions(token.COMMA, token.RBRACK)
	if p.tok != token.RBRACK {
		p.error(p.pos, fmt.Sprintf("closing part operator, expected ], got %v", p.tok))
	} else {
		p.next()
	}
	return (&next).apply(func(els ...form.Form) (form.Form, error) {
		els = append(els, args...)
		return op.Apply(els...)
	})
}

func functionApplication(els ...form.Form) (form.Form, error) {
	if len(els) == 0 {
		return form.Null, nil
	}

	head := els[0]
	sym, ok := head.(form.SymbolForm)

	if ok {
		return form.New(form.NewUnknownSymbolicHead(sym.Raw()), els[1:]...)
	}

	return form.New(form.NewFormHead(head), els[1:]...)
}

func postfixApplication(els ...form.Form) (form.Form, error) {
	if len(els) != 2 {
		return form.Null, errors.New("Postfix application requires exactly two operands")
	}

	head := els[1]
	arg := els[0]

	sym, ok := head.(form.SymbolForm)
	if ok {
		return form.New(form.NewUnknownSymbolicHead(sym.Raw()), arg)
	}

	return form.New(form.NewFormHead(head), arg)
}

func applyLevel1(els ...form.Form) (form.Form, error) {
	els = append(els, form.NewIntegerList(1))
	return form.New(form.NewUnknownSymbolicHead("Apply"), els...)
}

var (
	Part = newOp().name("Part").postfix().poly().token(token.LPART).complete(completePart)

	Composition      = newOp().name("Composition").left().infix().token(token.COMPOSITION)
	RightComposition = newOp().name("RightComposition").left().infix().token(token.RCOMPOSITION)

	Application = newOp().poly().postfix().token(token.LBRACK).apply(functionApplication).completeDelimited(token.COMMA, token.RBRACK)
	Prefix      = newOp().right().infix().token(token.PREFIX).apply(functionApplication)
	Postfix     = newOp().left().infix().token(token.POSTFIX).apply(postfixApplication)

	Map    = newOp().name("Map").left().infix().token(token.MAP)
	MapAll = newOp().name("MapAll").left().infix().token(token.MAP_ALL)
	Apply  = newOp().name("Apply").left().infix().token(token.APPLY)
	Apply2 = newOp().infix().token(token.APPLY2).left().apply(applyLevel1)

	Factorial  = newOp().name("Factorial").postfix().token(token.FAC)
	Factorial2 = newOp().name("Factorial2").postfix().token(token.FAC2)

	Power         = newOp().name("Power").right().infix().token(token.POW)
	UnaryAdd      = newOp().name("Add").prefix().token(token.ADD)
	UnarySubtract = newOp().name("Subtract").prefix().token(token.SUB)

	Cross    = newOp().name("Cross").infix().poly().token(token.CROSS)
	Dot      = newOp().name("Dot").infix().poly().token(token.DOT)
	Divide   = newOp().name("Divide").infix().left().token(token.DIV)
	Multiply = newOp().name("Multiply").infix().poly().token(token.MUL)
	Subtract = newOp().name("Subtract").infix().left().token(token.SUB)
	Add      = newOp().name("Add").infix().poly().token(token.ADD)

	Span = newOp().name("Span").infix().poly().token(token.SPAN)

	Equal        = newOp().name("Equal").infix().poly().token(token.EQU)
	Same         = newOp().name("Same").infix().poly().token(token.SAME)
	Unequal      = newOp().name("Unequal").infix().poly().token(token.NEQ)
	Different    = newOp().name("Different").infix().poly().token(token.DIFF)
	Less         = newOp().name("Less").infix().poly().token(token.LSS)
	Greater      = newOp().name("Greater").infix().poly().token(token.GTR)
	LessEqual    = newOp().name("LessEqual").infix().poly().token(token.LEQ)
	GreaterEqual = newOp().name("GreaterEqual").infix().poly().token(token.GEQ)

	Set           = newOp().name("Set").infix().left().token(token.SET)
	SetDelayed    = newOp().name("SetDelayed").infix().left().token(token.SET_DELAYED)
	OutSet        = newOp().name("OutSet").infix().left().token(token.OUT_SET)
	OutSetDelayed = newOp().name("OutSetDelayed").infix().left().token(token.OUT_SET_DELAYED)
	Rule          = newOp().name("Rule").infix().left().token(token.RULE)
	RuleDelayed   = newOp().name("RuleDelayed").infix().left().token(token.RULE_DELAYED)

	Block = newOp().name("Block").infix().poly().token(token.SEMICOLON)
)

var precedence = [][]*Operator{
	[]*Operator{Part},
	[]*Operator{Composition},
	[]*Operator{RightComposition},
	[]*Operator{Application, Prefix},
	[]*Operator{Map},
	[]*Operator{MapAll},
	[]*Operator{Apply},
	[]*Operator{Apply2},
	[]*Operator{Factorial, Factorial2},
	[]*Operator{Power},
	[]*Operator{UnaryAdd, UnarySubtract},
	[]*Operator{Cross},
	[]*Operator{Dot},
	[]*Operator{Multiply, Divide},
	[]*Operator{Add, Subtract},
	[]*Operator{Span},
	[]*Operator{Equal, Same, Unequal, Different, Less, Greater, LessEqual, GreaterEqual},
	[]*Operator{Rule, RuleDelayed},
	[]*Operator{Postfix},
	[]*Operator{Set, SetDelayed, OutSet, OutSetDelayed},
	[]*Operator{Block},
}
var MinPrecedence = 0
var MaxPrecedence = len(precedence) + 1

var Operators = map[OperatorKind]map[token.Token]*Operator{
	PrefixOperator:  map[token.Token]*Operator{},
	InfixOperator:   map[token.Token]*Operator{},
	PostfixOperator: map[token.Token]*Operator{},
}

func init() {
	var n = len(precedence)
	for i, sub := range precedence {
		for _, op := range sub {
			op.Precedence = n - i

			if _, ok := Operators[op.Kind][op.Token]; ok {
				panic("only one operator per token is allowed")
			}
			Operators[op.Kind][op.Token] = op
		}
	}
}
