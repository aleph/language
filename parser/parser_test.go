package parser

import (
	"fmt"
	"testing"

	"github.com/alecthomas/assert"
	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/language/scanner"
)

type context struct{}

func (c *context) GetKnownHeadName(id uint64) (name string, ok bool) {
	return "", false
}

func (c *context) GetKnownHeadID(name string) (id uint64, ok bool) {
	return 0, false
}

func testParse(t *testing.T, p *Parser, ctx form.Context, all bool, str string, expect form.Form) {
	p.Init([]byte(str), noBailOut, ctx)

	var x form.Form
	var errs scanner.ErrorList
	if all {
		x, errs = p.ParseAll()
	} else {
		x, errs = p.Parse()
	}
	for _, err := range errs {
		t.Errorf("%s [char %d]", err.Msg, err.Pos)
	}
	if len(errs) > 0 {
		t.Fail()
	}

	assert.Equal(t, fmt.Sprint(expect), fmt.Sprint(x))
	assert.Equal(t, expect, x)
}

func TestProgram(t *testing.T) {
	var ctx = new(context)
	var p = new(Parser)
	var str = `
		f[x_] := x^2
		g[x_] := Sin[x]
		y = 2;

		f[g[x]] == Sin[x]^2
	`
	var expect = Compound(
		Form("SetDelayed", Form("f", Form("Pattern", Symbol("x"), Form("Blank"))), Form("Power", Symbol("x"), Integer(2))),
		Form("SetDelayed", Form("g", Form("Pattern", Symbol("x"), Form("Blank"))), Form("Sin", Symbol("x"))),
		block(Form("Set", Symbol("y"), Integer(2)), form.Null),
		Form("Equal", Form("f", Form("g", Symbol("x"))), Form("Power", Form("Sin", Symbol("x")), Integer(2))),
	)

	testParse(t, p, ctx, true, str, expect)
}

func TestNotBasic(t *testing.T) {
	var ctx = new(context)
	var p = new(Parser)
	var cases = map[string]struct {
		str    string
		expect form.Form
	}{
		"f(x)/Blank": {"f[x_] := x^2", Form("SetDelayed", Form("f", Form("Pattern", Symbol("x"), Form("Blank"))), Form("Power", Symbol("x"), Integer(2)))},
		"Compound/A": {"(a;b)+c", Form("Add", block(Symbol("a"), Symbol("b")), Symbol("c"))},
		"Compound/B": {"a;b;c", block(Symbol("a"), Symbol("b"), Symbol("c"))},
		"f(x,y,z)":   {"f[x,y,z]", Form("f", Symbol("x"), Symbol("y"), Symbol("z"))},
		"List":       {"{x,y,z}", List(Symbol("x"), Symbol("y"), Symbol("z"))},
	}

	for name, test := range cases {
		t.Run(name, func(t *testing.T) { testParse(t, p, ctx, false, test.str, test.expect) })
	}
}

func TestRegression(t *testing.T) {
	var ctx = new(context)
	var p = new(Parser)
	var cases = map[string]struct {
		str    string
		expect form.Form
	}{
		"A": {"SetAttributes[f, Protected]\nf = 1", Compound(
			Form("SetAttributes", Symbol("f"), Symbol("Protected")),
			Form("Set", Symbol("f"), Integer(1)),
		)},
		"B": {"a*b/c", Compound(Form("Divide", Form("Multiply", Symbol("a"), Symbol("b")), Symbol("c")))},
		"C": {"a/b*c", Compound(Form("Multiply", Form("Divide", Symbol("a"), Symbol("b")), Symbol("c")))},
		"D": {"a+b*c/d^x/e*f^y", Compound(
			Form("Add",
				Symbol("a"),
				Form("Multiply",
					Form("Divide",
						Form("Divide",
							Form("Multiply",
								Symbol("b"),
								Symbol("c"),
							),
							Form("Power",
								Symbol("d"),
								Symbol("x"),
							),
						),
						Symbol("e"),
					),
					Form("Power",
						Symbol("f"),
						Symbol("y"),
					),
				),
			),
		)},
	}

	for name, test := range cases {
		t.Run(name, func(t *testing.T) { testParse(t, p, ctx, true, test.str, test.expect) })
	}
}

func TestBasic(t *testing.T) {
	var ctx = new(context)
	var p = new(Parser)
	var cases = map[string]struct {
		str    string
		expect form.Form
	}{
		"String/A":      {`"str"`, String("str")},
		"String/B":      {`"str\n"`, String("str\n")},
		"String/Raw/A":  {"`str`", String(`str`)},
		"String/Raw/B":  {"`str\\n`", String(`str\n`)},
		"Integer/0":     {"0", Integer(0)},
		"Real/0.":       {"0.", Real(0)},
		"Real/.0":       {".0", Real(0)},
		"Integer/1":     {"1", Integer(1)},
		"Real/1.":       {"1.", Real(1)},
		"Boolean/True":  {"True", Boolean(true)},
		"Boolean/False": {"False", Boolean(false)},
		"Symbol":        {"x", Symbol("x")},

		"ImplicitMultiply": {"x y", Form("Multiply", Symbol("x"), Symbol("y"))},
		"Multiply":         {"x*y", Form("Multiply", Symbol("x"), Symbol("y"))},
		"Add":              {"x+y", Form("Add", Symbol("x"), Symbol("y"))},
		"Power":            {"x^y", Form("Power", Symbol("x"), Symbol("y"))},
		"MultiplyPower":    {"a*x^y", Form("Multiply", Symbol("a"), Form("Power", Symbol("x"), Symbol("y")))},
		"AddMultiply":      {"a+b*c+d", Form("Add", Symbol("a"), Form("Multiply", Symbol("b"), Symbol("c")), Symbol("d"))},
		"MultiplyAdd":      {"a*b+c*d", Form("Add", Form("Multiply", Symbol("a"), Symbol("b")), Form("Multiply", Symbol("c"), Symbol("d")))},

		"Set":         {"x = y", Form("Set", Symbol("x"), Symbol("y"))},
		"SetDelayed":  {"x := y", Form("SetDelayed", Symbol("x"), Symbol("y"))},
		"Rule":        {"x -> y", Form("Rule", Symbol("x"), Symbol("y"))},
		"RuleDelayed": {"x :> y", Form("RuleDelayed", Symbol("x"), Symbol("y"))},

		"Part/A": {"x[[1]]", Form("Part", Symbol("x"), Integer(1))},
		"Part/B": {"x[[1;;2]]", Form("Part", Symbol("x"), Form("Span", Integer(1), Integer(2)))},

		"Blank/A":   {"_", Form("Blank")},
		"Blank/B":   {"_Integer", Form("Blank", Symbol("Integer"))},
		"Pattern/A": {"x_", Form("Pattern", Symbol("x"), Form("Blank"))},
		"Pattern/B": {"x_Integer", Form("Pattern", Symbol("x"), Form("Blank", Symbol("Integer")))},

		"Call":       {"f[x]", Form("f", Symbol("x"))},
		"Call/Empty": {"f[]", Form("f")},

		"Postfix": {"x // f // g", Form("g", Form("f", Symbol("x")))},
	}

	for name, test := range cases {
		t.Run(name, func(t *testing.T) { testParse(t, p, ctx, false, test.str, test.expect) })
	}
}

func Form(head string, exprs ...form.Form) form.Form {
	return form.Must(form.New(form.NewUnknownSymbolicHead(head), exprs...))
}

func Compound(exprs ...form.Form) form.Form { return Form("CompoundExpression", exprs...) }
func block(exprs ...form.Form) form.Form    { return Form("Block", exprs...) }
func List(exprs ...form.Form) form.Form     { return form.NewList(exprs...) }

func Integer(v int64) form.Form { return form.SmallIntegerForm(v) }
func Real(v float64) form.Form  { return form.SmallRealForm(v) }
func String(v string) form.Form { return form.StringForm(v) }
func Symbol(v string) form.Form { return form.SymbolForm(v) }
func Boolean(v bool) form.Form  { return form.BooleanForm(v) }
