package scanner

import (
	"testing"

	"github.com/alecthomas/assert"

	"gitlab.com/aleph-project/language/token"
)

var tokens = map[string]token.Token{
	// "∅":   token.NULL,
	// "∞":   token.INF,
	",":   token.COMMA,
	":":   token.PATTERN,
	";":   token.SEMICOLON,
	";;":  token.SPAN,
	"×":   token.CROSS,
	".":   token.DOT,
	"⋅":   token.DOT,
	"..":  token.REPEATED,
	"...": token.REPEATED_NULL,
	"~":   token.INFIX,
	"//":  token.POSTFIX,
	"@":   token.PREFIX,
	"@*":  token.COMPOSITION,
	"@@":  token.APPLY,
	"@@@": token.APPLY2,
	"/;":  token.CONDITION,
	"/:":  token.TAG_SET,
	"/.":  token.REPLACE_ALL,
	"/*":  token.RCOMPOSITION,
	"/@":  token.MAP,
	"//@": token.MAP_ALL,
	"//.": token.REPLACE_REPEATED,
	"?":   token.PATTERN_TEST,
	"|":   token.ALTERNATIVES,
	"&":   token.CONJUNCTIVES,
	"+":   token.ADD,
	"-":   token.SUB,
	"*":   token.MUL,
	"/":   token.DIV,
	"%":   token.MOD,
	"^":   token.POW,
	"!":   token.FAC,
	"!!":  token.FAC2,
	// "≪":   token.SHL,
	"<<": token.SHL,
	// "≫":   token.SHR,
	">>": token.SHR,
	// "±":   token.PLMN,
	// "∓":   token.PLMN,
	"¬":  token.NOT,
	"&&": token.AND,
	"∧":  token.AND,
	"||": token.OR,
	"∨":  token.OR,
	"⊻":  token.XOR,
	"⊕":  token.XOR,
	"&^": token.NAND,
	"⊼":  token.NAND,
	"|^": token.NOR,
	"⊽":  token.NOR,
	// "∀":   token.FORALL,
	// "∃":   token.EXISTS,
	// "∄":   token.NEXISTS,
	// "∈":   token.ELEM,
	// "∉":   token.NELEM,
	// "∋":   token.CONT,
	// "∌":   token.NCONT,
	// "∩":   token.JOIN,
	// "∪":   token.UNION,
	// "⊂":   token.SUBS,
	// "⊃":   token.SUPS,
	"_":   token.BLANK,
	"__":  token.BLANK_SEQ,
	"___": token.BLANK_NULL_SEQ,
	"=":   token.SET,
	"≔":   token.SET_DELAYED,
	":=":  token.SET_DELAYED,
	"=^":  token.OUT_SET,
	":=^": token.OUT_SET_DELAYED,
	// "→":   token.RULE,
	"->": token.RULE,
	// "↣":   token.RULE_DELAYED,
	":>": token.RULE_DELAYED,
	// "∣":   token.DIVS,
	// "∤":   token.NDIVS,
	// "∥":   token.PARA,
	// "∦":   token.NPARA,
	// "∝":   token.PROPORTIONAL,
	"==": token.EQU,
	// "≡":   token.SAME,
	"===": token.SAME,
	// "≠":   token.NEQ,
	"!=": token.NEQ,
	// "≢":   token.DIFF,
	"!==": token.DIFF,
	"<":   token.LSS,
	">":   token.GTR,
	// "≤":   token.LEQ,
	"<=": token.LEQ,
	// "≥":   token.GEQ,
	">=": token.GEQ,
	"(":  token.LPAREN,
	"[":  token.LBRACK,
	"{":  token.LBRACE,
	")":  token.RPAREN,
	"]":  token.RBRACK,
	"}":  token.RBRACE,
	"[[": token.LPART,
	// "]]": token.RPART,
}

func TestTokens(t *testing.T) {
	var s = new(Scanner)

	for str, expect := range tokens {
		t.Run(str, func(t *testing.T) {
			s.Init([]byte(str+" "), ScanDefault, func(pos int, msg string) {
				t.Fatalf("an error occurred at character %d: %s", pos, msg)
			})

			pos, tok, _ := s.Scan()
			assert.Equal(t, pos, 0, "expected a token at position 0, got %d", pos)
			assert.Equal(t, tok, expect, "expected token %v, got %v", expect, tok)
		})
	}
}

func TestRegression(t *testing.T) {
	var s = new(Scanner)
	var cases = map[string]struct {
		str    string
		expect []token.Token
	}{
		"A": {"SetAttributes[f, Protected]\nf = 1", []token.Token{
			token.SYMBOL, token.LBRACK, token.SYMBOL, token.COMMA, token.SYMBOL, token.RBRACK, token.NEWLINE,
			token.SYMBOL, token.SET, token.INTEGER,
		}},
	}

	for name, test := range cases {
		t.Run(name, func(t *testing.T) {
			s.Init([]byte(test.str), ScanDefault, func(pos int, msg string) {
				t.Fatalf("an error occurred at character %d: %s", pos, msg)
			})

			var tokens = []token.Token{}
			for {
				_, tok, _ := s.Scan()
				if tok == token.EOF {
					break
				}
				tokens = append(tokens, tok)
			}

			assert.Equal(t, test.expect, tokens)
		})
	}
}
