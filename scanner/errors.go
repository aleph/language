package scanner

type Error struct {
	Pos int
	Msg string
}

func (e Error) Error() string {
	return e.Msg
}

type ErrorList []*Error

func (l ErrorList) Len() int {
	return len(l)
}

func (l *ErrorList) Add(pos int, msg string) {
	*l = append(*l, &Error{pos, msg})
}

func (l *ErrorList) Clear() {
	*l = (*l)[0:0]
}
