package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/aleph-project/language/parser"
)

type context struct{}

func (c *context) GetKnownHeadName(id uint64) (name string, ok bool) {
	return "", false
}

func (c *context) GetKnownHeadID(name string) (id uint64, ok bool) {
	return 0, false
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	if len(os.Args) < 2 {
		fmt.Println("Usage: parse <expr>...")
		return
	}

	var parseMode = parser.ParseDefault
	var parser = new(parser.Parser)

	str := strings.Join(os.Args[1:], "\n")
	parser.Init([]byte(str), parseMode, new(context))

	exprs, errs := parser.ParseAll()
	for _, err := range errs {
		fmt.Printf("%s [char %d]\n", err.Msg, err.Pos)
	}
	if len(errs) > 0 {
		return
	}

	if exprs.Head().Name() != "CompoundExpression" {
		fmt.Printf("%v\n", exprs)
		return
	}

	for _, r := range exprs.GetElements() {
		fmt.Printf("%v\n", r)
	}
}
