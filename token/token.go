// nolint: golint
package token

import (
	"strconv"
)

// Token represents an aleph query language token
type Token int

const (
	// Special tokens
	ILLEGAL Token = iota
	EOF
	COMMENT
	WHITESPACE
	NEWLINE

	// Literals
	SYMBOL     // symbol
	INTEGER    // 1
	REAL       // 1.0
	STRING     // 'string'
	RAW_STRING // `string`
	NULL       // ∅
	INF        // ∞

	// Delimiters, etc
	COMMA  // ,
	LPAREN // (
	LBRACK // [
	LBRACE // {
	RPAREN // )
	RBRACK // ]
	RBRACE // }

	// Operators
	SEMICOLON        // ;
	LPART            // [[
	PATTERN          // :
	SPAN             // ;;
	CROSS            // ×
	DOT              // . or ⋅
	REPEATED         // ..
	REPEATED_NULL    // ...
	INFIX            // ~
	POSTFIX          // //
	PREFIX           // @
	COMPOSITION      // @*
	APPLY            // @@
	APPLY2           // @@@
	CONDITION        // /;
	TAG_SET          // /:
	REPLACE_ALL      // /.
	RCOMPOSITION     // /*
	MAP              // /@
	MAP_ALL          // //@
	REPLACE_REPEATED // //.
	PATTERN_TEST     // ?
	ALTERNATIVES     // |
	CONJUNCTIVES     // &

	ADD  // +
	SUB  // -
	MUL  // *
	DIV  // /
	MOD  // %
	POW  // ^
	FAC  // !
	FAC2 // !!
	SHL  // ≪ or <<
	SHR  // ≫ or >>
	PLMN // ± or ∓

	NOT  // ¬
	AND  // && or ∧
	OR   // || or ∨
	XOR  // ⊻ or ⊕
	XNOR // 
	NAND // &^ or ⊼
	NOR  // |^ or ⊽

	FORALL  // ∀
	EXISTS  // ∃
	NEXISTS // ∄
	ELEM    // ∈
	NELEM   // ∉
	CONT    // ∋
	NCONT   // ∌
	JOIN    // ∩
	UNION   // ∪
	SUBS    // ⊂
	SUPS    // ⊃

	BLANK          // _
	BLANK_SEQ      // __
	BLANK_NULL_SEQ // ___

	SET             // =
	SET_DELAYED     // ≔ or :=
	OUT_SET         // =^
	OUT_SET_DELAYED // :=^
	RULE            // → or ->
	RULE_DELAYED    // ↣ or :>

	DIVS         // ∣
	NDIVS        // ∤
	PARA         // ∥
	NPARA        // ∦
	PROPORTIONAL // ∝

	EQU  // ==
	SAME // ≡ or ===
	NEQ  // ≠ or !=
	DIFF // ≢ or !==
	LSS  // <
	GTR  // >
	LEQ  // ≤ or <=
	GEQ  // ≥ or >=

	TOKEN_COUNT
)

var names = [...]string{
	ILLEGAL:          "ILLEGAL",
	EOF:              "EOF",
	COMMENT:          "COMMENT",
	WHITESPACE:       "WHITESPACE",
	NEWLINE:          "NEWLINE",
	SYMBOL:           "SYMBOL",
	INTEGER:          "INTEGER",
	REAL:             "REAL",
	STRING:           "STRING",
	RAW_STRING:       "RAW_STRING",
	NULL:             "∅",
	INF:              "∞",
	COMMA:            ",",
	PATTERN:          ":",
	SEMICOLON:        ";",
	SPAN:             ";;",
	CROSS:            "×",
	DOT:              "⋅",
	REPEATED:         "..",
	REPEATED_NULL:    "...",
	INFIX:            "~",
	POSTFIX:          "//",
	PREFIX:           "@",
	COMPOSITION:      "@*",
	APPLY:            "@@",
	APPLY2:           "@@@",
	RCOMPOSITION:     "/*",
	MAP:              "/@",
	MAP_ALL:          "//@",
	CONDITION:        "/;",
	TAG_SET:          "/:",
	REPLACE_ALL:      "/.",
	REPLACE_REPEATED: "//.",
	PATTERN_TEST:     "?",
	ALTERNATIVES:     "|",
	CONJUNCTIVES:     "&",
	ADD:              "+",
	SUB:              "-",
	MUL:              "*",
	DIV:              "/",
	MOD:              "%",
	POW:              "^",
	FAC:              "!",
	FAC2:             "!!",
	SHL:              "≪",
	SHR:              "≫",
	PLMN:             "±",
	NOT:              "¬",
	AND:              "∧",
	OR:               "∨",
	XOR:              "⊻",
	XNOR:             "",
	NAND:             "⊼",
	NOR:              "⊽",
	FORALL:           "∀",
	EXISTS:           "∃",
	NEXISTS:          "∄",
	ELEM:             "∈",
	NELEM:            "∉",
	CONT:             "∋",
	NCONT:            "∌",
	JOIN:             "∩",
	UNION:            "∪",
	SUBS:             "⊂",
	SUPS:             "⊃",
	BLANK:            "_",
	BLANK_SEQ:        "__",
	BLANK_NULL_SEQ:   "___",
	SET:              "=",
	SET_DELAYED:      "≔",
	OUT_SET:          "=^",
	OUT_SET_DELAYED:  ":=^",
	RULE:             "→",
	RULE_DELAYED:     "↣",
	DIVS:             "∣",
	NDIVS:            "∤",
	PARA:             "∥",
	NPARA:            "∦",
	PROPORTIONAL:     "∝",
	EQU:              "==",
	SAME:             "≡",
	NEQ:              "≠",
	DIFF:             "≢",
	LSS:              "<",
	GTR:              ">",
	LEQ:              "≤",
	GEQ:              "≥",
	LPAREN:           "(",
	LBRACK:           "[",
	LBRACE:           "{",
	RPAREN:           ")",
	RBRACK:           "]",
	RBRACE:           "}",
	LPART:            "[[",
}

func (tok Token) String() string {
	var s = ""

	if 0 <= tok && tok < Token(len(names)) {
		s = names[tok]
	}
	if s == "" {
		s = "token(" + strconv.Itoa(int(tok)) + ")"
	}

	return s
}

func (tok Token) IsOneOf(toks ...Token) bool {
	for _, t := range toks {
		if tok == t {
			return true
		}
	}

	return false
}
